from test import client
import pytest

def test_get_description(client):
    response = client.get("/room/description")
    assert response.status_code == 200

def test_get_room_state(client, auth_token):
    headers = { 'Authorization': 'Bearer {}'.format(auth_token) }
    response = client.get("/room/examine", headers=headers)
    assert response.status_code == 200
    assert response.json["exits_locked"] == True

def test_solution(client, auth_token):
    headers = { 'Authorization': 'Bearer {}'.format(auth_token) }
    # examine desk
    response = client.get("/room/objects/1/examine", headers=headers)
    assert response.status_code == 200
    # use key on desk
    response = client.post("/room/objects/1/interact", json={'object_id': '3'}, headers=headers)
    assert response.status_code == 200
    # read diary
    response = client.get("/room/objects/4/examine", headers=headers)
    assert response.status_code == 200
    # use code on column
    response = client.post("/room/objects/2/interact", json={'parameter': '1234'}, headers=headers)
    assert response.status_code == 200
    assert response.json["exits_locked"] == False

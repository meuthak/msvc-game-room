from apiflask import HTTPTokenAuth
from app.models.roomState import RoomState

token_auth = HTTPTokenAuth()

@token_auth.verify_token
def verify_token(token):
    room_state = RoomState.verify_auth_token(token)
    if not room_state:
        return False
    return room_state
from apiflask import APIBlueprint

bp = APIBlueprint('room', __name__)

from app.room import routes


